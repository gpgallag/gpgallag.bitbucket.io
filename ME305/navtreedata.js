/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME305: Intro to Mechatronics (Fall 2020)", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Portfolio Details", "index.html#sec_details", null ],
    [ "Source Code Repository", "index.html#respository", null ],
    [ "Lab 0x01: Fibonacci Function", "lab0x01.html", [
      [ "Summary", "lab0x01.html#sec_lab0x01", null ],
      [ "Documentation", "lab0x01.html#lab0x01_documentation", null ],
      [ "Source Code", "lab0x01.html#lab0x01_source", null ]
    ] ],
    [ "Hw 0x00: Elevator Finite State Machine", "hw0x00.html", [
      [ "Summary", "hw0x00.html#sec_hw0x00", null ],
      [ "Documentation", "hw0x00.html#hw0x01_documentation", null ],
      [ "Source Code", "hw0x00.html#hw0x01_source", null ]
    ] ],
    [ "Lab 0x02: LED Finite State Machine", "lab0x02.html", [
      [ "Summary", "lab0x02.html#sec_lab0x02", null ],
      [ "Documentation", "lab0x02.html#lab0x02_documentation", null ],
      [ "Source Code", "lab0x02.html#lab0x02_source", null ]
    ] ],
    [ "Lab 0x03: Encoder Driver", "lab0x03.html", [
      [ "Summary", "lab0x03.html#sec_lab0x03", null ],
      [ "Encoder Finite-State Machine", "lab0x03.html#encoderTask", null ],
      [ "User Interface State-Transition Diagram", "lab0x03.html#userTask", null ],
      [ "Documentation", "lab0x03.html#lab0x03_documentation", null ],
      [ "Source Code", "lab0x03.html#lab0x03_source", null ]
    ] ],
    [ "Lab 0x04: Encoder User Interface", "lab0x04.html", [
      [ "Summary", "lab0x04.html#sec_lab0x04", null ],
      [ "User Interface State-Transition Diagram", "lab0x04.html#userInterface", null ],
      [ "Data Collection State-Transition Diagram", "lab0x04.html#dataCollect", null ],
      [ "User Interface Task Diagram", "lab0x04.html#UserInterfaceTaskDiagram", null ],
      [ "Expected Output", "lab0x04.html#UserInterfaceOutput", null ],
      [ "Documentation", "lab0x04.html#lab0x04_documentation", null ],
      [ "Source Code", "lab0x04.html#lab0x04_source", null ]
    ] ],
    [ "Lab 0x05: BLE User Interface", "lab0x05.html", [
      [ "Summary", "lab0x05.html#sec_lab0x05", null ],
      [ "Bluetooth Low Energy (BLE) Driver State-Transition Diagram", "lab0x05.html#BLEDriver", null ],
      [ "LED Driver State-Transition Diagram", "lab0x05.html#LEDDriver", null ],
      [ "LED User Interface Task Diagram", "lab0x05.html#LEDUITaskDiagram", null ],
      [ "Thunkable App", "lab0x05.html#lab0x05_app", null ],
      [ "Documentation", "lab0x05.html#lab0x05_documentation", null ],
      [ "Source Code", "lab0x05.html#lab0x05_source", null ]
    ] ],
    [ "Lab 0x06: DC Motors", "lab0x06.html", [
      [ "Summary", "lab0x06.html#sec_lab0x06", null ],
      [ "Closed-loop Feedback and PID Control", "lab0x06.html#Control", null ],
      [ "System Task Diagram", "lab0x06.html#SystemTaskDiagram", null ],
      [ "Front-end User Interface State-Transition Diagram", "lab0x06.html#InterfaceFront", null ],
      [ "Back-end User Interface State-Transition Diagram", "lab0x06.html#InterfaceRemote", null ],
      [ "Closed-loop Controller State-Transition Diagram", "lab0x06.html#TaskControl", null ],
      [ "Motor Step Response Plots and Gain Tuning", "lab0x06.html#MotorStep", null ],
      [ "Documentation", "lab0x06.html#lab0x06_documentation", null ],
      [ "Source Code", "lab0x06.html#lab0x06_source", null ]
    ] ],
    [ "Lab 0x07: Reference Tracking", "lab0x07.html", [
      [ "Summary", "lab0x07.html#sec_lab0x07", null ],
      [ "System Performance", "lab0x07.html#Performance", null ],
      [ "System Task Diagram", "lab0x07.html#SystemTaskDiagram07", null ],
      [ "Front-end User Interface State-Transition Diagram", "lab0x07.html#InterfaceFront07", null ],
      [ "Back-end User Interface State-Transition Diagram", "lab0x07.html#InterfaceRemote07", null ],
      [ "Closed-loop Controller State-Transition Diagram", "lab0x07.html#TaskControl07", null ],
      [ "Reference Tracking Results", "lab0x07.html#ReferenceResponse", null ],
      [ "Issues and Discussion", "lab0x07.html#Issues", null ],
      [ "Documentation", "lab0x07.html#lab0x07_documentation", null ],
      [ "Source Code", "lab0x07.html#lab0x07_source", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classmain_1_1DataCollect.html#a0b2f96f8a1b94d31459c826e46880638",
"taskControl07_8py.html#af1d6a1fdad400f9c0e98c1b9a78f04dd"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';