var annotated_dup =
[
    [ "BLEDriver", null, [
      [ "BLEDriver", "classBLEDriver_1_1BLEDriver.html", "classBLEDriver_1_1BLEDriver" ]
    ] ],
    [ "blinky", null, [
      [ "physicalLED", "classblinky_1_1physicalLED.html", "classblinky_1_1physicalLED" ],
      [ "virtualLED", "classblinky_1_1virtualLED.html", "classblinky_1_1virtualLED" ]
    ] ],
    [ "closedLoop", null, [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "elevator", null, [
      [ "Button", "classelevator_1_1Button.html", "classelevator_1_1Button" ],
      [ "MotorDriver", "classelevator_1_1MotorDriver.html", "classelevator_1_1MotorDriver" ],
      [ "TaskElevator", "classelevator_1_1TaskElevator.html", "classelevator_1_1TaskElevator" ]
    ] ],
    [ "encoderDriver", null, [
      [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "encoderTask", null, [
      [ "EncoderTask", "classencoderTask_1_1EncoderTask.html", "classencoderTask_1_1EncoderTask" ]
    ] ],
    [ "interfaceFront", null, [
      [ "InterfaceFront", "classinterfaceFront_1_1InterfaceFront.html", "classinterfaceFront_1_1InterfaceFront" ]
    ] ],
    [ "interfaceFront07", null, [
      [ "InterfaceFront07", "classinterfaceFront07_1_1InterfaceFront07.html", "classinterfaceFront07_1_1InterfaceFront07" ]
    ] ],
    [ "interfaceRemote", null, [
      [ "InterfaceRemote", "classinterfaceRemote_1_1InterfaceRemote.html", "classinterfaceRemote_1_1InterfaceRemote" ]
    ] ],
    [ "interfaceRemote07", null, [
      [ "InterfaceRemote07", "classinterfaceRemote07_1_1InterfaceRemote07.html", "classinterfaceRemote07_1_1InterfaceRemote07" ]
    ] ],
    [ "LEDDriver", null, [
      [ "LEDDriver", "classLEDDriver_1_1LEDDriver.html", "classLEDDriver_1_1LEDDriver" ]
    ] ],
    [ "main", null, [
      [ "DataCollect", "classmain_1_1DataCollect.html", "classmain_1_1DataCollect" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "taskControl", null, [
      [ "TaskControl", "classtaskControl_1_1TaskControl.html", "classtaskControl_1_1TaskControl" ]
    ] ],
    [ "taskControl07", null, [
      [ "TaskControl07", "classtaskControl07_1_1TaskControl07.html", "classtaskControl07_1_1TaskControl07" ]
    ] ],
    [ "UserInterface", null, [
      [ "UserInterface", "classUserInterface_1_1UserInterface.html", "classUserInterface_1_1UserInterface" ]
    ] ],
    [ "userTask", null, [
      [ "UserTask", "classuserTask_1_1UserTask.html", "classuserTask_1_1UserTask" ]
    ] ]
];