var classUserInterface_1_1UserInterface =
[
    [ "__init__", "classUserInterface_1_1UserInterface.html#aaf1f7d6213da2bb494e275f7ba55dfb0", null ],
    [ "convert_to_array", "classUserInterface_1_1UserInterface.html#a4e1e0d1b3cf72ebadda1a60b61066782", null ],
    [ "export_data", "classUserInterface_1_1UserInterface.html#ab63377332008145dbb6c2923b059e6fc", null ],
    [ "export_plot", "classUserInterface_1_1UserInterface.html#ae24305be94ee84e6877f3aa5ec17cc6d", null ],
    [ "run", "classUserInterface_1_1UserInterface.html#a8c06e6af9552fa983a2f035d055f99ba", null ],
    [ "transitionTo", "classUserInterface_1_1UserInterface.html#ae802d9d79ded24c3f045880a45185e67", null ],
    [ "array_position", "classUserInterface_1_1UserInterface.html#aacac16af19239e500a3efb0c37d1e318", null ],
    [ "array_time", "classUserInterface_1_1UserInterface.html#ae84cd84c142a5a77006f1aefd4b59e1a", null ],
    [ "conversion_factor", "classUserInterface_1_1UserInterface.html#aefbe83f7d01f4d96d8e0f0b9fdde21c2", null ],
    [ "current_time", "classUserInterface_1_1UserInterface.html#ae2c356cc820c7b63b6dca637c1c98e42", null ],
    [ "interval", "classUserInterface_1_1UserInterface.html#a47d28320c75ecac491bdfee625fd68cb", null ],
    [ "is_negative", "classUserInterface_1_1UserInterface.html#af18496d70d7169bf34e69534358498f7", null ],
    [ "next_time", "classUserInterface_1_1UserInterface.html#a587e60dfae6ee4c2916f41a941aa41d2", null ],
    [ "output", "classUserInterface_1_1UserInterface.html#aefce92d490dbe3e0417992f320ee99f1", null ],
    [ "ref_array", "classUserInterface_1_1UserInterface.html#a54e3afdce254587c31bb69bff8081236", null ],
    [ "serial_port", "classUserInterface_1_1UserInterface.html#a62218665ca5d79678bfedc357c0e550e", null ],
    [ "start_time", "classUserInterface_1_1UserInterface.html#a98cb2fef1c1b57e0bffd9f125289b2ba", null ],
    [ "state", "classUserInterface_1_1UserInterface.html#aa9fad0f92c5499e16bc4a6b85abfd8e4", null ],
    [ "string_number", "classUserInterface_1_1UserInterface.html#af349b38f071a0af112fb5622a74da042", null ],
    [ "task_finished", "classUserInterface_1_1UserInterface.html#a2c2af0a5fcb3e11808477539e496817c", null ],
    [ "value_writer", "classUserInterface_1_1UserInterface.html#a23e5e02403fd0799e9464ce03421f056", null ]
];