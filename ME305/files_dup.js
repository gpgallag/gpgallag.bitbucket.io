var files_dup =
[
    [ "BLEDriver.py", "BLEDriver_8py.html", [
      [ "BLEDriver", "classBLEDriver_1_1BLEDriver.html", "classBLEDriver_1_1BLEDriver" ]
    ] ],
    [ "blinky.py", "blinky_8py.html", [
      [ "virtualLED", "classblinky_1_1virtualLED.html", "classblinky_1_1virtualLED" ],
      [ "physicalLED", "classblinky_1_1physicalLED.html", "classblinky_1_1physicalLED" ]
    ] ],
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "elevator.py", "elevator_8py.html", [
      [ "TaskElevator", "classelevator_1_1TaskElevator.html", "classelevator_1_1TaskElevator" ],
      [ "Button", "classelevator_1_1Button.html", "classelevator_1_1Button" ],
      [ "MotorDriver", "classelevator_1_1MotorDriver.html", "classelevator_1_1MotorDriver" ]
    ] ],
    [ "encoderDriver.py", "encoderDriver_8py.html", [
      [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", [
      [ "EncoderTask", "classencoderTask_1_1EncoderTask.html", "classencoderTask_1_1EncoderTask" ]
    ] ],
    [ "fibonFunction.py", "fibonFunction_8py.html", "fibonFunction_8py" ],
    [ "interfaceFront.py", "interfaceFront_8py.html", "interfaceFront_8py" ],
    [ "interfaceFront07.py", "interfaceFront07_8py.html", "interfaceFront07_8py" ],
    [ "interfaceRemote.py", "interfaceRemote_8py.html", [
      [ "InterfaceRemote", "classinterfaceRemote_1_1InterfaceRemote.html", "classinterfaceRemote_1_1InterfaceRemote" ]
    ] ],
    [ "interfaceRemote07.py", "interfaceRemote07_8py.html", [
      [ "InterfaceRemote07", "classinterfaceRemote07_1_1InterfaceRemote07.html", "classinterfaceRemote07_1_1InterfaceRemote07" ]
    ] ],
    [ "LEDDriver.py", "LEDDriver_8py.html", [
      [ "LEDDriver", "classLEDDriver_1_1LEDDriver.html", "classLEDDriver_1_1LEDDriver" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "motorDriver.py", "motorDriver_8py.html", [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "taskControl.py", "taskControl_8py.html", "taskControl_8py" ],
    [ "taskControl07.py", "taskControl07_8py.html", "taskControl07_8py" ],
    [ "UserInterface.py", "UserInterface_8py.html", "UserInterface_8py" ],
    [ "userTask.py", "userTask_8py.html", [
      [ "UserTask", "classuserTask_1_1UserTask.html", "classuserTask_1_1UserTask" ]
    ] ]
];