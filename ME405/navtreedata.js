/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME405: Mechatronics (Winter 2021)", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Portfolio Details", "index.html#sec_details", null ],
    [ "Source Code Repository", "index.html#respository", null ],
    [ "Lab 0x01: Vendotron Finite State Machine", "lab0x01.html", [
      [ "Summary", "lab0x01.html#sec_lab0x01_summary", null ],
      [ "Design Requirements", "lab0x01.html#sec_lab0x01_design_requirements", null ],
      [ "User Inputs", "lab0x01.html#sec_lab0x01_inputs", null ],
      [ "Vendotron State-Transition Diagram", "lab0x01.html#sec_lab0x01_FSD", null ],
      [ "Documentation", "lab0x01.html#lab0x01_documentation", null ],
      [ "Source Code", "lab0x01.html#lab0x01_source", null ]
    ] ],
    [ "Lab 0x02: Think Fast!", "lab0x02.html", [
      [ "Summary", "lab0x02.html#sec_lab0x02_summary", null ],
      [ "Design Requirements", "lab0x02.html#sec_lab0x02_design_requirements", null ],
      [ "Documentation", "lab0x02.html#lab0x02_documentation", null ],
      [ "Source Code", "lab0x02.html#lab0x02_source", null ]
    ] ],
    [ "Lab 0x03: Pushing the Right Buttons", "lab0x03.html", [
      [ "Summary", "lab0x03.html#sec_lab0x03_summary", null ],
      [ "Design Requirements", "lab0x03.html#sec_lab0x03_design_requirements", null ],
      [ "Results", "lab0x03.html#sec_lab0x03_results", null ],
      [ "UI_Front State-Transition Diagram", "lab0x03.html#sec_lab0x03_UI_FRONT_FSD", null ],
      [ "UI_Back State-Transition Diagram", "lab0x03.html#sec_lab0x03_UI_BACK_FSD", null ],
      [ "Documentation", "lab0x03.html#lab0x03_documentation", null ],
      [ "Source Code", "lab0x03.html#lab0x03_source", null ]
    ] ],
    [ "Lab 0x04: Hot or Not?", "lab0x04.html", [
      [ "Summary", "lab0x04.html#sec_lab0x04_summary", null ],
      [ "I2C Communications", "lab0x04.html#sec_lab0x04_I2C", null ],
      [ "Design Requirements", "lab0x04.html#sec_lab0x04_design_requirements", null ],
      [ "Results", "lab0x04.html#sec_lab0x04_results", null ],
      [ "Documentation", "lab0x04.html#lab0x04_documentation", null ],
      [ "Source Code", "lab0x04.html#lab0x04_source", null ]
    ] ],
    [ "Lab 0x05: Feeling Tipsy?", "lab0x05.html", [
      [ "Source Code", "lab0x05.html#lab0x05_source", null ]
    ] ],
    [ "Lab 0x06: Simulation or Reality?", "lab0x06.html", [
      [ "Source Code", "lab0x06.html#lab0x06_source", null ]
    ] ],
    [ "Lab 0x07: Feeling Touchy", "lab0x07.html", [
      [ "Summary", "lab0x07.html#sec_lab0x07_summary", null ],
      [ "Hardware Assembly", "lab0x07.html#lab0x07_hardware_assembly", null ],
      [ "Resistive Touch Panel", "lab0x07.html#lab0x07_resistive_touch_panel", null ],
      [ "Calibration", "lab0x07.html#lab0x07_calibration", null ],
      [ "Results", "lab0x07.html#lab0x07_results", null ],
      [ "Documentation", "lab0x07.html#lab0x07_documentation", null ],
      [ "Source Code", "lab0x07.html#lab0x07_source", null ]
    ] ],
    [ "Lab 0x08: Term Project Part I", "lab0x08.html", [
      [ "Summary", "lab0x08.html#lab0x08_summary", null ],
      [ "Hardware Connections", "lab0x08.html#lab0x08_hardware_connections", null ],
      [ "Fault Detection", "lab0x08.html#lab0x08_fault_detection", null ],
      [ "Results", "lab0x08.html#lab0x08_results", null ],
      [ "Documentation", "lab0x08.html#lab0x08_documentation", null ],
      [ "Source Code", "lab0x08.html#lab0x08_source", null ]
    ] ],
    [ "Lab 0x09: Term Project Part II", "lab0x09.html", [
      [ "Summary", "lab0x09.html#lab0x09_summary", null ],
      [ "Controller Tuning", "lab0x09.html#lab0x09_controller_tuning", null ],
      [ "Challenges", "lab0x09.html#lab0x09_challenges", null ],
      [ "Results", "lab0x09.html#lab0x09_results", null ],
      [ "Discussion", "lab0x09.html#lab0x09_discussion", null ],
      [ "Task Controller State-Transition Diagram", "lab0x09.html#lab0x09_FSD", null ],
      [ "Controller Task Diagram", "lab0x09.html#lab0x09_Task_Diagram", null ],
      [ "Video Demo", "lab0x09.html#lab0x09_video_demo", null ],
      [ "Documentation", "lab0x09.html#lab0x09_documentation", null ],
      [ "Source Code", "lab0x09.html#lab0x09_source", null ],
      [ "Documentation for the IMU driver", "lab0x09.html#bno055", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classtaskController_1_1TaskController.html#ab2302a1a8d74a6a7014ffdf94b36bfef"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';